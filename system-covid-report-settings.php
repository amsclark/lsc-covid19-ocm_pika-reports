<?php
/***********************************/
/* Pika CMS (C) 2010 Pika Software */
/* http://pikasoftware.com         */
/*                                 */
/* Modified December 2019          */
/* By Metatheria, LLC              */ 
/* https://metatheria.solutions    */
/***********************************/

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once('pika-danio.php');
pika_init();
require_once('pikaSettings.php');
require_once('pikaMisc.php');
require_once('pikaTempLib.php');

$main_html = $html = array();
$base_url = pl_settings_get('base_url');

$main_html['page_title'] = $page_title = "Covid-19 Report Settings";
$main_html['nav'] = "<a href=\"{$base_url}/\">%%[branding]%% Home</a> &gt; 
						<a href=\"{$base_url}/site_map.php\">Site Map</a> &gt; 
						{$page_title}";

$action = pl_grab_post('action');

if (!pika_authorize('system',array()))
{
	$main_html['content'] = "Access denied";
	
	$default_template = new pikaTempLib('templates/default.html',$main_html);
	$buffer = $default_template->draw();
	pika_exit($buffer);
}

switch ($action)
{
	case 'update':
                // fund codes
                pl_settings_set('lsc_covid_supplemental_fund_code', pl_grab_post('lsc_covid_supplemental_fund_code'));
                if(is_array(pl_grab_post('other_lsc_codes_list'))) {
                  pl_settings_set('other_lsc_codes_list', implode(',',pl_grab_post('other_lsc_codes_list')));
                }
                else
                {
                  pl_settings_set('other_lsc_codes_list', pl_grab_post('other_lsc_codes_list'));
                }
                // office codes
                if(is_array(pl_grab_post('pai_office_codes_list'))) {
                  pl_settings_set('pai_office_codes_list', implode(',',pl_grab_post('pai_office_codes_list')));
                }
                else
                {
                  pl_settings_set('pai_office_codes_list', pl_grab_post('pai_office_codes_list'));
                }
                //gender codes
                pl_settings_set('men_code', pl_grab_post('men_code'));
                pl_settings_set('women_code', pl_grab_post('women_code'));
                if(is_array(pl_grab_post('other_gender_codes_list')))
                {
                  pl_settings_set('other_gender_codes_list', implode(',',pl_grab_post('other_gender_codes_list')));
                }
                else
                { 
                  pl_settings_set('other_gender_codes_list', pl_grab_post('other_gender_codes_list'));
                }
                // veteran codes
                pl_settings_set('veteran_code', pl_grab_post('veteran_code'));
                pl_settings_set('non_veteran_code', pl_grab_post('non_veteran_code'));
                //ethnicity codes
                if(is_array(pl_grab_post('asian_codes_list'))) 
                {
                  pl_settings_set('asian_codes_list', implode(',',pl_grab_post('asian_codes_list')));
                }
                else
                { 
                  pl_settings_set('asian_codes_list', pl_grab_post('asian_codes_list'));
                }
                if (is_array(pl_grab_post('black_codes_list'))) 
                { 
                  pl_settings_set('black_codes_list', implode(',',pl_grab_post('black_codes_list'))); 
                }
                else { pl_settings_set('black_codes_list', pl_grab_post('black_codes_list')); }
                if (is_array(pl_grab_post('hispanic_codes_list'))) 
                { 
                  pl_settings_set('hispanic_codes_list', implode(',',pl_grab_post('hispanic_codes_list')));
                }
                else { pl_settings_set('hispanic_codes_list', pl_grab_post('hispanic_codes_list')); }
                if (is_array(pl_grab_post('native_codes_list'))) 
                {
                  pl_settings_set('native_codes_list', implode(',',pl_grab_post('native_codes_list')));
                }
                else { pl_settings_set('native_codes_list', pl_grab_post('native_codes_list')); }
                if (is_array(pl_grab_post('other_codes_list'))) 
                {
                  pl_settings_set('other_codes_list', implode(',',pl_grab_post('other_codes_list')));
                }
                else { pl_settings_set('other_codes_list', pl_grab_post('other_codes_list')); }
                if (is_array(pl_grab_post('white_codes_list'))) 
                {
                  pl_settings_set('white_codes_list', implode(',',pl_grab_post('white_codes_list')));
                }
                else { pl_settings_set('white_codes_list', pl_grab_post('white_codes_list')); }
                //closing codes 
                if (is_array(pl_grab_post('close_a_list'))) {
                  pl_settings_set('close_a_list', implode(',',pl_grab_post('close_a_list')));
                } else { pl_settings_set('close_a_list', pl_grab_post('close_a_list')); }

                if (is_array(pl_grab_post('close_b_list'))) {
                  pl_settings_set('close_b_list', implode(',',pl_grab_post('close_b_list')));
                } else { pl_settings_set('close_b_list', pl_grab_post('close_b_list')); }


                if (is_array(pl_grab_post('close_f_list'))) {
                  pl_settings_set('close_f_list', implode(',',pl_grab_post('close_f_list')));
                } else { pl_settings_set('close_f_list', pl_grab_post('close_f_list')); }


                if (is_array(pl_grab_post('close_g_list'))) {
                  pl_settings_set('close_g_list', implode(',',pl_grab_post('close_g_list')));
                } else { pl_settings_set('close_g_list', pl_grab_post('close_g_list')); }


                if (is_array(pl_grab_post('close_h_list'))) {
                  pl_settings_set('close_h_list', implode(',',pl_grab_post('close_h_list')));
                } else { pl_settings_set('close_h_list', pl_grab_post('close_h_list')); }


                if (is_array(pl_grab_post('close_ia_list'))) {
                  pl_settings_set('close_ia_list', implode(',',pl_grab_post('close_ia_list')));
                } else { pl_settings_set('close_ia_list', pl_grab_post('close_ia_list')); }


                if (is_array(pl_grab_post('close_ib_list'))) {
                  pl_settings_set('close_ib_list', implode(',',pl_grab_post('close_ib_list')));
                } else { pl_settings_set('close_ib_list', pl_grab_post('close_ib_list')); }


                if (is_array(pl_grab_post('close_ic_list'))) {
                  pl_settings_set('close_ic_list', implode(',',pl_grab_post('close_ic_list')));
                } else { pl_settings_set('close_ic_list', pl_grab_post('close_ic_list')); }


                 if (is_array(pl_grab_post('close_k_list'))) {
                   pl_settings_set('close_k_list', implode(',',pl_grab_post('close_k_list')));
                } else { pl_settings_set('close_k_list', pl_grab_post('close_k_list')); }
                
                if (is_array(pl_grab_post('close_l_list'))) {
                  pl_settings_set('close_l_list', implode(',',pl_grab_post('close_l_list')));
                } else { pl_settings_set('close_l_list', pl_grab_post('close_l_list')); }
		            pl_settings_save();
		
	default:
                $html['lsc_covid_supplemental_fund_code'] = pl_settings_get('lsc_covid_supplemental_fund_code');
                $html['other_lsc_codes_list'] = explode(',',pl_settings_get('other_lsc_codes_list'));
                $html['pai_office_codes_list'] = explode(',',pl_settings_get('pai_office_codes_list'));
                $html['men_code'] = pl_settings_get('men_code');
                $html['women_code'] = pl_settings_get('women_code');
                $html['other_gender_codes_list'] = explode(',',pl_settings_get('other_gender_codes_list'));
                $html['veteran_code'] = pl_settings_get('veteran_code');
                $html['non_veteran_code'] = pl_settings_get('non_veteran_code');
                $html['asian_codes_list'] = explode(',',pl_settings_get('asian_codes_list'));
                $html['black_codes_list'] = explode(',',pl_settings_get('black_codes_list'));
                $html['hispanic_codes_list'] = explode(',',pl_settings_get('hispanic_codes_list'));
                $html['native_codes_list'] = explode(',',pl_settings_get('native_codes_list'));
                $html['other_codes_list'] = explode(',',pl_settings_get('other_codes_list'));
                $html['white_codes_list'] = explode(',',pl_settings_get('white_codes_list')); 
                $html['close_a_list'] = explode(',',pl_settings_get('close_a_list'));
                $html['close_b_list'] = explode(',',pl_settings_get('close_b_list'));
                $html['close_f_list'] = explode(',',pl_settings_get('close_f_list'));
                $html['close_g_list'] = explode(',',pl_settings_get('close_g_list'));
                $html['close_h_list'] = explode(',',pl_settings_get('close_h_list'));
                $html['close_ia_list'] = explode(',',pl_settings_get('close_ia_list'));
                $html['close_ib_list'] = explode(',',pl_settings_get('close_ib_list'));
                $html['close_ic_list'] = explode(',',pl_settings_get('close_ic_list'));
                $html['close_k_list'] = explode(',',pl_settings_get('close_k_list'));  
                $html['close_l_list'] = explode(',',pl_settings_get('close_l_list'));
		            $template = new pikaTempLib('subtemplates/system-covid-report-settings.html',$html);
                $template->addMenu('office',pl_menu_get('office'));
                $template->addMenu('gender',pl_menu_get('gender'));
                $template->addMenu('funding',pl_menu_get('funding'));
                $template->addMenu('yes_no',pl_menu_get('yes_no'));
                $template->addMenu('ethnicity',pl_menu_get('ethnicity'));
                $template->addMenu('close_code_2008',pl_menu_get('close_code_2008'));
		$main_html['content'] = $template->draw();
		
		break;
}

// add in report list for sidebar
$reports = pikaMisc::reportList(true);
$y = ""; 
foreach ($reports as $z) 
{
        $y .= "<li>{$z}</li>\n";
}
$main_html['report_list'] = $y; 


$default_template = new pikaTempLib('templates/default.html',$main_html);
$buffer = $default_template->draw();
pika_exit($buffer);

?>