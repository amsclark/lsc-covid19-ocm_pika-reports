<?php
/***********************************/
/* Pika CMS (C) Pika Software      */
/* http://pikasoftware.com         */
/*                                 */
/* Modified January 2020           */
/* By Metatheria, LLC              */ 
/* https://metatheria.solutions    */
/***********************************/
chdir('../../');

require_once('pika-danio.php'); 
pika_init();
require_once('pikaTempLib.php');
require_once('pikaMisc.php');
require_once('pikaSettings.php');

$report_title = 'LSC Covid-19 Report';
$report_name = "covid";

$base_url = pl_settings_get('base_url');
if(!pika_report_authorize($report_name)) {
	$main_html = array();
	$main_html['base_url'] = $base_url;
	$main_html['page_title'] = $report_title;
	$main_html['nav'] = "<a href=\"{$base_url}/\">Pika Home</a>
    				  &gt; <a href=\"{$base_url}/reports/\">Reports</a> 
    				  &gt; $report_title";
	$main_html['content'] = "You are not authorized to run this report";

	$buffer = pl_template('templates/default.html', $main_html);
	pika_exit($buffer);
}

// check here if the module is configured correctly and direct user to set it up if not.
if(strlen(pl_settings_get('women_code')) == 0 
  or strlen(pl_settings_get('lsc_covid_supplemental_fund_code')) == 0
  or strlen(pl_settings_get('other_lsc_codes_list')) == 0 
  or strlen(pl_settings_get('pai_office_codes_list')) == 0 
  or strlen(pl_settings_get('men_code')) == 0 
  or strlen(pl_settings_get('other_gender_codes_list')) == 0 
  or strlen(pl_settings_get('white_codes_list')) == 0
  or strlen(pl_settings_get('other_codes_list')) == 0
  or strlen(pl_settings_get('asian_codes_list')) == 0
  or strlen(pl_settings_get('black_codes_list')) == 0
  or strlen(pl_settings_get('hispanic_codes_list')) == 0
  or strlen(pl_settings_get('native_codes_list')) == 0
  or strlen(pl_settings_get('close_a_list')) == 0   
  or strlen(pl_settings_get('close_b_list')) == 0 
  or strlen(pl_settings_get('close_f_list')) == 0 
  or strlen(pl_settings_get('close_g_list')) == 0 
  or strlen(pl_settings_get('close_h_list')) == 0 
  or strlen(pl_settings_get('close_ia_list')) == 0 
  or strlen(pl_settings_get('close_ib_list')) == 0 
  or strlen(pl_settings_get('close_ic_list')) == 0 
  or strlen(pl_settings_get('close_k_list')) == 0 
  or strlen(pl_settings_get('close_l_list')) == 0 
  )
{
  $main_html = array();
  $main_html['base_url'] = $base_url;
  $main_html['page_title'] = $report_title;
  $main_html['nav'] = "<a href=\"{$base_url}/\">Pika Home</a>
    &gt; <a href=\"{$base_url}/reports/\">Reports</a>
    &gt; $report_title";
  $main_html['content'] = "Missing code mappings. Please configure the covid 19 reporting module before attempting to run.";
  $buffer = pl_template('templates/default.html', $main_html);
  pika_exit($buffer);
}

$menu_limit = array('100'=>'100','500'=>'500','1000'=>'1000');

$a = array();
$a['report_name'] = $report_name;
$a['open_date_begin'] = '1/1/' . (date('Y'));
$a['open_date_end'] =  '12/31/' . (date('Y'));
//$a['limit'] = '1000';



$main_html = array();
$reports = pikaMisc::reportList(true);
$y = "";
foreach ($reports as $z)
{
        $y .= "<li>{$z}</li>\n";
}
$main_html['report_list'] = $y;
$main_html['base_url'] = $base_url;
$main_html['page_title'] = $report_title;
$main_html['nav'] = "<a href=\"{$base_url}/\">Pika Home</a>
      &gt; <a href=\"{$base_url}/reports/\">Reports</a> &gt; $report_title";
$template = new pikaTempLib("reports/{$report_name}/form.html", $a);
$template->addMenu('limit',$menu_limit);
$main_html['content'] = $template->draw();

$default_template = new pikaTempLib('templates/default.html', $main_html);
$buffer = $default_template->draw();
pika_exit($buffer);


?>
