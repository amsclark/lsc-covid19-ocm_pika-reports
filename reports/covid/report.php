<?php
/***********************************/
/* Pika CMS (C) Pika Software      */
/* http://pikasoftware.com         */
/*                                 */
/* Modified February 2020          */
/* By Metatheria, LLC              */ 
/* https://metatheria.solutions    */
/***********************************/


chdir('../../');

require_once ('pika-danio.php'); 
pika_init();
require_once('pikaTempLib.php');
require_once('pikaUser.php');
require_once('pikaFlags.php');
require_once('pikaSettings.php');

$report_title = 'LSC Covid-19 Report';
$report_name = "covid";

$base_url = pl_settings_get('base_url');
if(!pika_report_authorize($report_name)) {
	$main_html = array();
	$main_html['base_url'] = $base_url;
	$main_html['page_title'] = $report_title;
	$main_html['nav'] = "<a href=\"{$base_url}/\">Pika Home</a>
    				  &gt; <a href=\"{$base_url}/reports/\">Reports</a> 
    				  &gt; $report_title";
	$main_html['content'] = "You are not authorized to run this report";

	$buffer = pl_template('templates/default.html', $main_html);
	pika_exit($buffer);
}

//$report_format = pl_grab_post('report_format');
$report_format = 'csv';
$open_date_begin = pl_grab_post('open_date_begin');
$open_date_end = pl_grab_post('open_date_end');
$close_date_begin = pl_grab_post('close_date_begin');
$close_date_end = pl_grab_post('close_date_end');
//$show_sql = pl_grab_post('show_sql'); 
$reportmode = pl_grab_post('reportmode');
$menu_problem = pl_menu_get('problem_2008');
$menu_case_closing_code = pl_menu_get('close_code_2008');
$lsc_vet_codes = array("1"=>"V","0"=>"N");
$lsc_covid_supplemental_fund_code = pl_settings_get('lsc_covid_supplemental_fund_code');
$other_lsc_codes_list = explode(",",pl_settings_get('other_lsc_codes_list'));
$pai_office_codes_list = explode(",",pl_settings_get('pai_office_codes_list'));
$men_code = explode(",",pl_settings_get('men_code'));
$women_code = explode(",",pl_settings_get('women_code'));
$other_gender_codes_list = explode(",",pl_settings_get('other_gender_codes_list'));
$asian_codes_list = explode(",",pl_settings_get('asian_codes_list'));
$black_codes_list = explode(",",pl_settings_get('black_codes_list'));
$hispanic_codes_list = explode(",",pl_settings_get('hispanic_codes_list'));
$native_codes_list = explode(",",pl_settings_get('native_codes_list'));
$white_codes_list = explode(",",pl_settings_get('white_codes_list'));
$other_codes_list = explode(",",pl_settings_get('other_codes_list'));
$close_a_list = explode(",",pl_settings_get('close_a_list'));
$close_b_list = explode(",",pl_settings_get('close_b_list'));
$close_f_list = explode(",",pl_settings_get('close_f_list'));
$close_g_list = explode(",",pl_settings_get('close_g_list'));
$close_h_list = explode(",",pl_settings_get('close_h_list'));
$close_ia_list = explode(",",pl_settings_get('close_ia_list'));
$close_ib_list = explode(",",pl_settings_get('close_ib_list'));
$close_ic_list = explode(",",pl_settings_get('close_ic_list'));
$close_k_list = explode(",",pl_settings_get('close_k_list'));
$close_l_list = explode(",",pl_settings_get('close_l_list'));  

$base_url = pl_settings_get('base_url');

if ('csv' == $report_format)
{
	require_once ('plCsvReportTable.php');
	require_once ('plCsvReport.php');
	$t = new plCsvReport();
}

else
{
	require_once ('plHtmlReportTable.php');
	require_once ('plHtmlReport.php');
	$t = new plHtmlReport();
}


// run the report

$sql = "SELECT cases.*, contacts.*
		FROM cases
		LEFT JOIN contacts ON cases.client_id = contacts.contact_id
		WHERE c19_case = 1";


// earlier version of pika use these two lines instead:
// $safe_open_date_begin = mysql_real_escape_string(pl_date_mogrify($open_date_begin));
// $safe_open_date_end = mysql_real_escape_string(pl_date_mogrify($open_date_end));
// later versions use these:
$safe_open_date_begin = DB::escapeString(pl_date_mogrify($open_date_begin));
$safe_open_date_end = DB::escapeString(pl_date_mogrify($open_date_end));

if ($open_date_begin && $open_date_end) {
	// $t->add_parameter('Cases Opened Between',$open_date_begin . " - " . $open_date_end);
	$sql .= " AND open_date >= '{$safe_open_date_begin}' AND open_date <= '{$safe_open_date_end}'";
} elseif ($open_date_begin) {
	// $t->add_parameter('Opened After',$open_date_begin);
	$sql .= " AND open_date >= '{$safe_open_date_begin}'";
} elseif ($open_date_end) {
	// $t->add_parameter('Opened Before',$open_date_end);
	$sql .= " AND open_date <= '{$safe_open_date_end}'";
}





$clb = pl_date_mogrify($close_date_begin);
$cle = pl_date_mogrify($close_date_end);
//  earlier versions of pika use these two lines instead:
//  $safe_clb = mysql_real_escape_string($clb);
//  $safe_cle = mysql_real_escape_string($cle);
//  later versions use these 
$safe_clb = DB::escapeString($clb);
$safe_cle = DB::escapeString($cle);

if ($clb) 
{
        //$t->add_parameter('Closed On or After', $close_date_begin);
        $sql .= " AND close_date >= '{$safe_clb}'";
}

if ($cle) 
{
        //$t->add_parameter('Closed On or Before', $close_date_end);
        $sql .= " AND close_date <= '{$safe_cle}'";
}








$sql .= " ORDER BY open_date ASC";


$t->title = $report_title;
$t->set_header(array('Funding Source Code','Staffing Code','Problem Code','Closing Code','Gender Code','Veteran Status Code','Ethnicity Code','Year of Birth'));

$limit_count = 0;
// earlier versions of pike use this line:
// $result = mysql_query($sql) or trigger_error("SQL: " . $sql . " Error: " . mysql_error());
// later versions of pika use this line:
$result = DB::query($sql) or trigger_error("SQL: " . $sql . " Error: " . DB::error());

// earlier version of pika use this line:
// while ($row = mysql_fetch_assoc($result))
// later version of pika use this line
while ($row = DBResult::fetchRow($result))
{
  $rpt_row = array();
  // recode Pika-specific fund codes to LSC's 001/002/003 scheme for LSC CARES ACT/LSC Other/Non-LSC respectively
  if($row['funding'] == $lsc_covid_supplemental_fund_code) 
  {
    $rpt_row['funding'] = "001";
  } elseif(in_array($row['funding'],$other_lsc_codes_list)) {
    $rpt_row['funding'] = "002";
  } else {
    $rpt_row['funding'] = "003";
  }
  // recode whatever the organization uses for PAI office code to "P" and all other offices to "S"
  if(in_array($row['office'],$pai_office_codes_list)) {
    $rpt_row['office'] = "P";
  } else {
    $rpt_row['office'] = "S";
  }
  $rpt_row['problem'] = $row['problem'];
  //$rpt_row['close_code'] = ucwords(strtolower($row['close_code'])); // two letter close codes in Pika are all capital, whereas the codes provided by lsc for covid reporting are mandated to be in camelcase
    //changing this because some organizations ahve non-compliant closing codes.
    if(in_array($row['close_code'],$close_a_list)) { $rpt_row['close_code'] = "A"; }
    elseif(in_array($row['close_code'],$close_b_list)) { $rpt_row['close_code'] = "B"; }
    elseif(in_array($row['close_code'],$close_f_list)) { $rpt_row['close_code'] = "F"; }
    elseif(in_array($row['close_code'],$close_g_list)) { $rpt_row['close_code'] = "G"; }
    elseif(in_array($row['close_code'],$close_h_list)) { $rpt_row['close_code'] = "H"; }
    elseif(in_array($row['close_code'],$close_ia_list)) { $rpt_row['close_code'] = "Ia"; }
    elseif(in_array($row['close_code'],$close_ib_list)) { $rpt_row['close_code'] = "Ib"; }
    elseif(in_array($row['close_code'],$close_ic_list)) { $rpt_row['close_code'] = "Ic"; }
    elseif(in_array($row['close_code'],$close_k_list)) { $rpt_row['close_code'] = "K"; }
    elseif(in_array($row['close_code'],$close_l_list)) { $rpt_row['close_code'] = "L"; }
    else { $rpt_row['close_code'] = ""; }
  // recode whatever scheme the org uses for gender into LSC's men/women/other/unknown
  if(in_array($row['gender'],$men_code)) {
    $rpt_row['gender'] = "M";
  } elseif(in_array($row['gender'],$women_code)) {
    $rpt_row['gender'] = "W";
  } elseif(in_array($row['gender'],$other_gender_codes_list)) {
    $rpt_row['gender'] = "O";
  } else {
    $rpt_row['gender'] = "U";
  }
  $rpt_row['veteran_household'] = (strlen($row['veteran_household']) > 0 ? pl_array_lookup($row['veteran_household'],$lsc_vet_codes): "N");
  // recode the organization's ethnicity categories into Asian non-hispanic, Black non-hispanic, Hispanic, Native non-hispanic, white non-hispanic, and unknown
  if(in_array($row['ethnicity'],$asian_codes_list)){
    $rpt_row['ethnicity'] = "A";
  } elseif(in_array($row['ethnicity'],$black_codes_list)) {
    $rpt_row['ethnicity'] = "B";
  } elseif(in_array($row['ethnicity'],$hispanic_codes_list)) {
    $rpt_row['ethnicity'] = "H";
  } elseif(in_array($row['ethnicity'],$native_codes_list)) {
    $rpt_row['ethnicity'] = "N";
  } elseif(in_array($row['ethnicity'],$white_codes_list)) {
    $rpt_row['ethnicity'] = "W";
  } elseif(in_array($row['ethnicity'],$other_codes_list)) {
    $rpt_row['ethnicity'] = "O";
  } else {
    $rpt_row['ethnicity'] = "U";
  }
  if (strlen($row['birth_date']) > 0)
  {
    $rpt_row['birth_date'] = explode('-',$row['birth_date'])[0];
  }
  else {
    $rpt_row['birth_date'] = "1900";
  }
  $t->add_row($rpt_row);
}

/*
if($show_sql) {
	$t->set_sql($sql);
}
*/

$t->display();	
exit();

?>
