# Installation instructions #

*It is recommended that the instructions below be used in conjunction with the [installation and configuration walkthrough video](https://www.youtube.com/watch?v=9myofxcJ54k)*

***New:** Please also check out the [video instructions](https://www.youtube.com/watch?v=_Cm9XjBVz74) for how to save and format the generated report for upload to the LSC reporting portal.*

There are a couple database fields that you need to add manually, along with their corresponding UI elements before installing the actual reporting module. I recommend using Percona toolkit to add the necessary fields.

Before you attempt the online schema change with percona-toolkit, you should make sure you have enough disk space, because with the way the tool works, it will cause the database to temporarily take up twice as much space as normal. If you run out of disk space, nobody will be able to use your Pika until the issue is corrected, and it can be tricky to correct. You can use `df -h` to check disk space statistics.

1. If you are going to add fields rather than re-purpose an existing field, and if not already installed, install percona-toolkit:

CentOS/RedHat:
`sudo yum install percona-toolkit`

Debian-based distros:
`sudo apt-get install percona-toolkit`

2. You can then test out the field additions with:
`pt-online-schema-change --dry-run -uroot --ask-pass  --alter "ADD COLUMN c19_case tinyint(4)" D=cms,t=cases`

and

`pt-online-schema-change --dry-run -uroot --ask-pass --alter "ADD COLUMN c19_time tinyint(4)" D=cms,t=activities`

You will need to substitute in **your database name**, if it is not named cms. You will also need the root password to your database, as you will be asked to enter it.

If you get an error about not being able to connect to the socket, it may be because the socket file is located somewhere other than the standard location. For example, I had to specify `--socket=/opt/bitnami/mysql/tmp/mysql.sock` to get it to work on an AWS Lightsail LAMP image because it's based on bitnami. 

If everything looks okay, and both commands show the dry runs completed without errors, change `--dry-run` to `--execute` and run the commands again. 

The commands will take a while to run, but when they finish, the last lines should say "Succesfully altered " followed by the database and table names. You should now be able to see that the new fields are visible in Megareport. 

3. The next step is going to be to add the user interface elements for the corresponding new fields. I recommend making a copy of the template files with extension .bak before modifying them. We are going to be modifying `case-info.html` and `activity.html`. If you have never modified these templates before, you should first copy them to the cms-custom/subtemplates/ folder for your Pika server and edit them there.

`cp ../cms/subtemplates/case-info.html subtemplates/`
`cp ../cms/subtemplates/activity.html subtemplates/`

4. Now let's edit each file and add in the template code for the dropdowns. This is going to be `%%[c19_case,yes_no]%%` for the cases table field and `%%[c19_time,yes_no]%%` for the activities table field. Be careful not to put a space after the comma. 

With the `activities.html` template, it can be a little confusing to find the right area where the matters template code is, but it's going to be in the section that starts with %%[begin:L]%%. I recommend putting the dropdown right below the "Service Provided" dropdown before the `</td>` tag. Add a text label on the line above and line breaks.

With the case-info.html template, I think a logical place to put it would be near the problem code dropdown and funding code dropdown. But of course you can put it anywhere on this or another tab. Again, add a text label and line breaks as well. 

5. At this point, you should be able track covid-related cases and non-case matters. However, LSC also wants you to add a funding code for use where LSC Cares Act funds are directly funding a case or non-case matters. And to be clear, the report pulls based on these two new fields, it does not pull based on the fund code. So you will need to train your staff so that even if CARES Act funding has been selected, they still need to use the "covid-related" dropdown.

So you will want to go to Menus and Special Tables, to the "funding" table, and add in a new fund code for LSC Cares Act as well. 

6. Now we are ready to download the report files. Go back to the regular `cms/` directory rather than the `cms-custom/` directory to download these, since they are going to be new. The four report files should go in a new folder called "covid" within "reports." The settings screen .html template should go in subtemplates and the settings screen php file should go in cms/ like the rest of the system-*.php files. 

7. You will also want to edit the Site Map template so that there will be a link to the editor that you just downloaded as well.

`cp cms/subtemplates/site_map.html cms-custom/subtemplates/`

I think a good place for the link is right after the menus and special tables editor. You can add a line like this. 
`<li><a href="%%[base_url]%%/system-covid-report-settings.php">Covid-19 Report Settings</a></li>`

8. After this, you will need to go to the Covid-19 Report Settings system screen and create your mappings of your own OCM/Pika encodings to LSC's. If you don't complete this mapping in full, OCM/Pika will not let you run the report. 

9. The report generates csv format. This is just for case reporting. LSC has other requirements for non-case covid reporting, and this data is to be reporting on a form in GrantEase, so there is no custom report for it. However, with the new field and checkbox on the LSC other services screen, you will be able to use MegaReport to identify this non-case time for ease of reporting. Feel free to reach out to me if you have questions.


